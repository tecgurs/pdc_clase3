<?php
/**
 * @Filename: Factory.php
 * @Description:
 * @CreatedAt: 18/08/19 16:19
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg;


use Tg\Db\Mysql;

class Factory
{
    public static function mysql(): Mysql
    {
        $config = include BASE_PATH . '/config.php';

        return new Mysql(
            $config['db']['host'],
            $config['db']['dbname'],
            $config['db']['user'],
            $config['db']['pass']
        );
    }
}
