<?php
/**
 * @Filename: FileInput.php
 * @Description:
 * @CreatedAt: 16/09/19 15:24
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Validation;


class FileInput implements Element
{
    /** @var string */
    private $name;
    /** @var string */
    private $message;

    /**
     * FileInput constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function validate(): bool
    {
        if (!isset($_FILES[$this->name])) {
            $this->message = "No se recibió el archivo correspondiente a {$this->name}";
            return false;
        }



        var_dump($_FILES[$this->name]); exit;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getMessage(): string
    {
        if (empty($this->message)) {
            throw new Exception('No hay mensaje');
        }
        return $this->message;
    }


}
