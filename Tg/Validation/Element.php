<?php
/**
 * @Filename: Element.php
 * @Description:
 * @CreatedAt: 16/09/19 14:41
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Validation;


interface Element
{
    public function validate(): bool ;

    public function getMessage(): string ;
}
