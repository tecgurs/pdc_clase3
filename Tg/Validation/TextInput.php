<?php
/**
 * @Filename: TextInput.php
 * @Description:
 * @CreatedAt: 16/09/19 14:47
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Validation;


class TextInput implements Element
{
    /** @var string */
    private $name;
    /** @var string */
    private $message;
    /** @var string */
    private $regexPattern;

    /**
     * TextInput constructor.
     * @param string $name
     * @param string $regexPattern
     */
    public function __construct(string $name, string $regexPattern = '')
    {
        $this->name = $name;
        $this->regexPattern = $regexPattern;
    }

    public function validate(): bool
    {
        if (!isset($_POST[$this->name])) {
            $this->message = "No se recibió la variable POST {$this->name}";
            return false;
        }
        $value = strip_tags(trim($_POST[$this->name]));
        if (empty($value)) {
            $this->message = "La variable POST {$this->name} viene vacía";
            return false;
        }

        if (!empty($this->regexPattern)) {
            if (!preg_match($this->regexPattern, $value)) {
                $this->message = "La variable POST {$this->name} no comple con el patrón regex";
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getMessage(): string
    {
        if (empty($this->message)) {
            throw new Exception('No hay mensaje');
        }
        return $this->message;
    }

}
