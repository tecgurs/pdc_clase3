<?php
/**
 * @Filename: SelectInput.php
 * @Description:
 * @CreatedAt: 16/09/19 15:13
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Validation;


class SelectInput implements Element
{
    /** @var string */
    private $name;
    /** @var array  */
    private $validOptions = [];
    /** @var string */
    private $message;

    /**
     * SelectInput constructor.
     * @param string $name
     * @param array $validOptions
     */
    public function __construct(string $name, array $validOptions = [])
    {
        $this->name = $name;
        $this->validOptions = $validOptions;
    }

    public function validate(): bool
    {
        if (!isset($_POST[$this->name])) {
            $this->message = "No se recibió la variable POST {$this->name}";
            return false;
        }
        $value = strip_tags(trim($_POST[$this->name]));

        if (empty($this->validOptions)) {
            return true;
        }

        if (in_array($value, $this->validOptions, true)) {
            return true;
        }

        $this->message = "El valor de la variable POST {$this->name} no pertenece a las opciones válidas";
        return false;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getMessage(): string
    {
        if (empty($this->message)) {
            throw new Exception('No hay mensaje');
        }
        return $this->message;
    }

}
