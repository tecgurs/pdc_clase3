<?php
/**
 * @Filename: Form.php
 * @Description:
 * @CreatedAt: 16/09/19 14:41
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Validation;


class Form
{
    private $elements = [];
    /** @var string */
    private $message;

    /**
     * @return string
     * @throws Exception
     */
    public function getMessage(): string
    {
        if (empty($this->message)) {
            throw new Exception('No hay mensaje');
        }
        return $this->message;
    }

    public function pushElement(Element $element): Form
    {
        $this->elements[] = $element;

        return $this;
    }

    public function validate() {
        /** @var Element $element */
        foreach ($this->elements as $element) {
            if (!$element->validate()) {
                $this->message = $element->getMessage();
                return false;
            }
        }

        return true;
    }
}
