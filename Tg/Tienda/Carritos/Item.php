<?php

namespace Tg\Tienda\Carritos;


use Tg\Db\Exception;
use Tg\Tienda\Models\Carritos;

class Item
{
    /** @var int */
    private $id;
    /** @var string */
    private $sessionId;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $carritoId
     * @return Item
     * @throws Exception
     */
    public static function readFromDb(int $carritoId): Item
    {
        /** @var Carritos $model */
        $model = Carritos::findFirstById($carritoId);

        return self::readFromModel($model);
    }

    public static function readFromModel(Carritos $model): Item
    {
        $item = new self();
        $item->id = $model->getId();
        $item->sessionId = $model->getSessionId();

        return $item;
    }

    public static function findBySessionId(string $sessionId): Item
    {
        /** @var Carritos $carritoModelExistente */
        $carritoModelExistente = Carritos::findFirst(
            'session_id = :session_id',
            ['session_id' => $sessionId]
        );

        if ($carritoModelExistente) {
            return self::readFromModel($carritoModelExistente);
        }

        $carritoModelNuevo = new Carritos($sessionId);
        $carritoModelNuevo->save();

        return self::findBySessionId($sessionId);
    }
}
