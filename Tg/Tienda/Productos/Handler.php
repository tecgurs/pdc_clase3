<?php

namespace Tg\Tienda\Productos;


use Tg\Db\Exception as DbException;
use Tg\Tienda\Exception;
use Tg\Tienda\Models\Producto;

class Handler
{
    /**
     * @return string
     * @throws DbException
     * @throws Exception
     */
    public function getHtmlList(): string
    {
        $resultset = Producto::find();
        if ($resultset->count() == 0) {
            throw new Exception('No se encontraron productos');
        }

        $list = '';
        foreach ($resultset->toArray() as $productoModel) {
            $productoItem = Item::readFromModel($productoModel);
            $list .= $productoItem->generateHtmlCard();
        }

        return $list;
    }

}
