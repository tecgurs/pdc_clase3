<?php
/**
 * @Filename: ValidatorFactory.php
 * @Description:
 * @CreatedAt: 16/09/19 14:42
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\BasicCrud;


use Tg\Validation\FileInput;
use Tg\Validation\Form;
use Tg\Validation\SelectInput;
use Tg\Validation\TextInput;

class ValidatorFactory
{
    public static function create(): Form
    {
        $form = new Form();
        $form->pushElement(new TextInput('nombre'));
        $form->pushElement(new TextInput('precio', "#(\d+(\.\d+)?)#"));
        $form->pushElement(new SelectInput('unidad', ['kilo', 'bolsa']));
        $form->pushElement(new FileInput('foto'));

        return $form;
    }
}
