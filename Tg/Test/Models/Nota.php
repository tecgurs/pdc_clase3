<?php
/**
 * @Filename: Nota.php
 * @Description:
 * @CreatedAt: 19/08/19 10:32
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Test\Models;

use stdClass;
use Tg\Db\Model;

class Nota extends Model
{
    protected static $tableName = 'notas';

    /** @var string */
    protected $titulo;
    /** @var string */
    protected $texto;

    protected $fields = ['titulo', 'texto'];

    /**
     * Nota constructor.
     * @param string $titulo
     * @param string $texto
     */
    public function __construct(string $titulo, string $texto)
    {
        $this->titulo = $titulo;
        $this->texto = $texto;
    }

    /**
     * @return string
     */
    public function getTitulo(): string
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return Nota
     */
    public function setTitulo(string $titulo): Nota
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTexto(): string
    {
        return $this->texto;
    }

    /**
     * @param string $texto
     * @return Nota
     */
    public function setTexto(string $texto): Nota
    {
        $this->texto = $texto;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMtime(): string
    {
        return $this->mtime;
    }

    /**
     * @return string
     */
    public function getCtime(): string
    {
        return $this->ctime;
    }

    protected static function readFromStdClass(stdClass $object): Nota
    {
        $nota = new self(
            $object->titulo,
            $object->texto
        );
        $nota->setFromStdClass($object);

        return $nota;
    }
}
