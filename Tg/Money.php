<?php
/**
 * @Filename: Money.php
 * @Description:
 * @CreatedAt: 19/08/19 8:36
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg;


class Money
{
    const FACTOR_IVA = 1.16;

    /** @var int En centavos sin IVA */
    private $centavos;

    private $hasIva = false;

    private function __construct()
    {
    }

    public function withIva(): Money
    {
        $this->hasIva = true;

        return $this;
    }

    public function getString(string $prefix = '', $factor = 1): string
    {
        $output = $this->hasIva ? $this->centavos * self::FACTOR_IVA : $this->centavos;;
        return $prefix . number_format($output * $factor / 100, 2);
    }

    public function getFloat(): float
    {
        $output = $this->hasIva ? $this->centavos * self::FACTOR_IVA : $this->centavos;
        return $output / 100;
    }

    public function getCentavos(): int
    {
        if (!$this->hasIva) {
            return $this->centavos;
        }

        return (int) ($this->centavos * self::FACTOR_IVA);
    }

    public function incrementCentavos(int $centavos): Money
    {
        $this->centavos += $centavos;

        return $this;
    }

    public static function fromString(string $monto): Money
    {
        return self::fromFloat((float) $monto);
    }

    public static function fromFloat(float $monto): Money
    {
        $money = new self();
        $money->centavos = (int) (100 * $monto);

        return $money;
    }

    public static function fromCentavos(int $centavos): Money
    {
        $money = new self();
        $money->centavos = $centavos;

        return $money;
    }
}
