<?php
/**
 * @Filename: Exception.php
 * @Description:
 * @CreatedAt: 18/08/19 14:58
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Db;


class Exception extends \Exception
{

}
