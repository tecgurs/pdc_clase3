<?php
/**
 * @Filename: Model.php
 * @Description:
 * @CreatedAt: 18/08/19 16:06
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Db;


use PDO;
use PDOException;
use PDOStatement;
use stdClass;
use Tg\Db\Exception as DbException;
use Tg\Factory;

abstract class Model
{
    /** @var string */
    protected static $tableName;

    /** @var int */
    protected $id;
    /** @var string */
    protected $mtime;
    /** @var string */
    protected $ctime;

    /** @var PDO */
    private $mysql;
    /** @var string */
    private $message = '';

    protected $fields = [];

    protected function setFromStdClass(stdClass $object)
    {
        $this->id = $object->id;
        $this->mtime = $object->mtime;
        $this->ctime = $object->ctime;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        $message = $this->message;
        $this->message = '';

        return $message;
    }

    /**
     * @throws Exception
     */
    public function delete()
    {
        $tableName = static::$tableName;
        $query = "DELETE FROM {$tableName} WHERE id = :id";
        $bind = [':id' => $this->id];

        $mysql = $this->requireMysql();
        $statement = $mysql->prepare($query);
        $statement->execute($bind);
        $this->id = null;
        $this->mtime = null;
        $this->ctime = null;
    }

    public function save(): bool
    {
        $this->message = '';
        try {
            if ($this->id) {
                $this->update();
                return true;
            }
            $this->create();
            return true;
        } catch (DbException $e) {
            $this->message = 'DbException' . $e->getMessage();
            return false;
        } catch (PDOException $e) {
            $this->message = 'PDOException' . $e->getMessage();
            return false;
        }
    }

    /**
     * @param string $where
     * @param array $bind
     * @param string $order
     * @return Resultset
     * @throws Exception
     */
    public static function find(string $where = '', array $bind = [], string $order = ''): Resultset
    {
        $sentence = self::generateFindSentence($where, $bind, $order);

        $filas = $sentence->fetchAll(PDO::FETCH_OBJ);

        $resultset = new Resultset();
        foreach ($filas as $fila) {
            $resultset->pushModel(static::readFromStdClass($fila));
        }

        return $resultset;
    }

    /**
     * @param string $where
     * @param array $bind
     * @param string $order
     * @return Model
     * @throws DbException
     */
    public static function findFirst(string $where = '', array $bind = [], string $order = ''): Model
    {
        $sentence = self::generateFindSentence($where, $bind, $order);

        $objetoFila = $sentence->fetch(PDO::FETCH_OBJ);

        if (!$objetoFila) {
            throw new Exception("No se encontraron modelos");
        }

        return static::readFromStdClass($objetoFila);

    }

    /**
     * @param int $id
     * @return Model
     * @throws DbException
     */
    public static function findFirstById(int $id): Model
    {
        $sentence = self::generateFindSentence('id = :id', [':id' => $id]);

        $objetoFila = $sentence->fetch(PDO::FETCH_OBJ);
        if (!$objetoFila) {
            throw new Exception("No existe model para el Id {$id}");
        }

        return static::readFromStdClass($objetoFila);
    }

    protected static abstract function readFromStdClass(stdClass $object);

    /*protected abstract function generateBindArray(bool $isUpdate = false);*/
    protected function generateBindArray(bool $isUpdate = false): array
    {
        $bind = $isUpdate ? [':id' => $this->id] : [];
        /** @var string $field */
        foreach ($this->fields as $field) {
            $bind[":{$field}"] = $this->$field;
        }

        return $bind;
    }

    /**
     * @throws DbException
     * @throws PDOException
     */
    private function create()
    {
        $mysql = $this->requireMysql();

        $sentence = $mysql->prepare($this->generateQueryForCreate());
        $sentence->execute($this->generateBindArray());
        $this->id = $this->mysql->lastInsertId();
    }

    /**
     * @throws DbException
     * @throws PDOException
     */
    private function update()
    {
        $mysql = $this->requireMysql();

        $sentence = $mysql->prepare($this->generateQueryForUpdate());
        $sentence->execute($this->generateBindArray(true));
    }

    /**
     * @param string $where
     * @param array $bind
     * @param string $order
     * @return PDOStatement
     * @throws DbException
     */
    private static function generateFindSentence(string $where = '', array $bind = [], string $order = ''): PDOStatement
    {
        $sqlWhere = empty($where) ? '' : "WHERE {$where}";
        $sqlOrder = empty($order) ? '' : "ORDER BY {$order}";
        $tableName = static::$tableName;

        //$producto = new self();
        $mysql = Factory::mysql();
        $query = <<<sql
SELECT *
FROM {$tableName}
{$sqlWhere}
{$sqlOrder}
sql;
        $sentence = $mysql->prepare($query);
        $sentence->execute($bind);

        return $sentence;
    }

    private function generateQueryForCreate(): string
    {
        $tableName = static::$tableName;
        /** @noinspection SqlResolve */
        /** @noinspection SqlInsertValues */
        $query = <<<sql
INSERT INTO {$tableName}(ctime {$this->generateFieldList()})
VALUES (NOW() {$this->generateFieldList(':')})
sql;

        return $query;
    }

    private function generateQueryForUpdate(): string
    {
        $tableName = static::$tableName;
        /** @noinspection SqlResolve */
        $query = <<<sql
UPDATE {$tableName}
SET mtime = NOW() {$this->generateFieldUpdateList()}
WHERE id = :id
sql;

        return $query;
    }

    private function generateFieldList(string $fieldPrefix = ''): string
    {
        $list = '';
        /** @var string $field */
        foreach ($this->fields as $field) {
            $list .= ', ' . $fieldPrefix . $field;
        }

        return $list;
    }

    private function generateFieldUpdateList(): string
    {
        $list = '';
        /** @var string $field */
        foreach ($this->fields as $field) {
            $list .= ", {$field} = :{$field}";
        }

        return $list;
    }


    private function requireMysql(): Mysql
    {
        if (!is_object($this->mysql)) {
            $this->mysql = Factory::mysql();
        }

        return $this->mysql;
    }

}
