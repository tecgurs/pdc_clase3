<?php
/**
 * @Filename: Mysql.php
 * @Description:
 * @CreatedAt: 18/08/19 14:47
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Db;


use PDO;
use PDOException;
use PDOStatement;

class Mysql
{
    /** @var string */
    private $host;
    /** @var string */
    private $dbname;
    /** @var string */
    private $user;
    /** @var string */
    private $pass;

    /** @var PDO */
    private $pdo;

    /**
     * Mysql constructor.
     * @param string $host
     * @param string $dbname
     * @param string $user
     * @param string $pass
     */
    public function __construct(string $host, string $dbname, string $user, string $pass)
    {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * @param string $mysqlQuery
     * @return PDOStatement
     * @throws Exception
     */
    public function prepare(string $mysqlQuery): PDOStatement
    {
        $this->connect();
        try {
            return $this->pdo->prepare($mysqlQuery);
        } catch (PDOException $e) {
            throw new Exception('Error al preparar la query: ' . $e->getMessage());
        }
    }

    /**
     * @param string $mysqlQuery
     * @return PDOStatement
     * @throws Exception
     */
    public function query(string $mysqlQuery): PDOStatement
    {
        $this->connect();
        try {
            return $this->pdo->query($mysqlQuery);
        } catch (PDOException $e) {
            throw new Exception('Error al preparar la query: ' . $e->getMessage());
        }
    }

    private function isConnected(): bool
    {
        if (is_object($this->pdo)) {
            return true;
        }
        return false;
    }

    /**
     * @throws Exception
     */
    private function connect()
    {
        if ($this->isConnected()) {
            return;
        }

        try {
            $this->pdo = new PDO(
                "mysql:dbname={$this->dbname};host={$this->host};charset=utf8",
                $this->user,
                $this->pass,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION] // Para que pdo lance exceptions
            );
        } catch (PDOException $e) {
            throw new Exception('Falló la conexión a Mysql: ' . $e->getMessage());
        }
    }
}
