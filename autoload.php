<?php

const BASE_PATH = __DIR__;

spl_autoload_register( function($className) {
    $subPath = str_replace("\\",'/',$className);
    //include_once __DIR__ . '/' . $subpath . '.php';
    include_once __DIR__ . "/{$subPath}.php";
} );

session_start();
