<?php

use Rcc\Html5\Property;
use Tg\Tienda\Models\Articulos;
use Tg\Tienda\Models\Carritos;
use Tg\Tienda\Models\Producto;
use Tg\Tienda\Productos\Handler as ProductosHandler;

require_once 'autoload.php';

//$sessionId = session_id();

//$carrito = new Carritos($sessionId);
//$carrito->save();
/*
$articulo = new Articulos(1, 2, 1);



$articuloExistente = Articulos::findFirst(
        'carritoId = :carritoId AND productoId = :productoId',
        [
            'carritoId' => 1,
            'productoId' => 2,
        ]
);

var_dump($articuloExistente);

$articulo->save();

var_dump($articulo); exit;
*/
//var_dump($sessionId); exit;


// Controller
$handler = new ProductosHandler();
$htmlProductos = $handler->getHtmlList();

// View or Template
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="title" content="Tienda virtual usando PHP, JS y Materialize">
  <meta name="description" content="Guía paso a paso para crear una tienda virtual">
  <meta name="keywords" content="Tienda virtual, e-commerce, PHP, JS, Materialize">

  <title>Tienda virtual</title>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection">

</head>
<body>

<nav class="light-blue darken-4" role="navigation">
  <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Tienda Virtual</a>
    <ul class="right hide-on-med-and-down">
      <li><a href="#">Contacto</a></li>
      <li>
        <a href="#">
          Carrito
          <span id="carrito-resumen" class="new badge red cart-badge" data-badge-caption="artículos">3</span>
        </a>
      </li>
    </ul>

    <ul id="nav-mobile" class="sidenav">
      <li><a href="#">Contacto</a></li>
      <li>
        <a href="#">
          Carrito
          <span class="new badge red cart-badge" data-badge-caption="artículos">3</span>
        </a>
      </li>

    </ul>
    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>

<div class="container">
  <div class="section">
    <div class="row">

      <!--  CARRITO -->
      <div id="carrito-lista" class="col s12 m5 push-m7 l4 push-l8" >

        <div class="card">
          <div class="card-content">
            <h5>No hay productos en el carrito</h5>
          </div>
        </div>

      </div>


      <!-- PRODUCTOS -->
      <div class="col s12 m7 pull-m5 l8 pull-l4">
        <div class="row">

          <?php echo $htmlProductos ?>

        </div>
      </div>


    </div>

  </div>
  <br><br>
</div>


<footer class="page-footer light-blue darken-4">
  <div class="container">
    <div class="row">

      <div class="col l7 s12">
        <h5 class="white-text">Company Bio</h5>
        <p class="grey-text text-lighten-4">Somos una empresa con años de experiencia en nuestro rubro en México y el extranjero.</p>
      </div>

      <div class="col l3 s12">
        <h5 class="white-text">Contacto</h5>
        <ul>
          <li class="grey-text text-lighten-4"><i class="material-icons">phone</i> 81 0000 0000</li>
          <li class="grey-text text-lighten-4"><i class="material-icons">email</i> info@example.com</li>
        </ul>
      </div>

      <div class="col l2 s12">
        <h5 class="white-text">Síguenos</h5>
        <ul>
          <li><a href="#" class="white-text">Facebook</a></li>
          <li><a href="#" class="white-text">Twitter</a></li>
          <li><a href="#" class="white-text">LinkedIn</a></li>
          <li><a href="#" class="white-text">Instagram</a></li>
        </ul>
      </div>

    </div>
  </div>

  <div class="footer-copyright">
    <div class="container">
      <i class="material-icons">copyright</i> 2019 Tec Gurus
    </div>
  </div>

</footer>

<!--  Scripts-->
<script src="js/jquery-3.3.1.js"></script>
<script src="js/materialize.js"></script>
<script src="js/prototypes.js"></script>
<script src="js/front.js"></script>
<script>

    $(document).ready(function () {
        front.construct('/tg/clase3/');
    });

</script>
</body>
</html>
