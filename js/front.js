
let front = (function () {

    let construct = function (urlBase) {
        console.log('Inside front contruct');
        $('.sidenav').sidenav();
        productosCarrito.init(urlBase);
    };

    return {
        construct: construct,
    };
})();

let carrito_old = (function () {

    let urlHome;

    let construct = function (urlBase) {
        urlHome = urlBase;
        console.log(urlHome);
    };

    let productos = (function () {

        let container = $('.carrito-agregar');

    })();

    let resumen = (function () {

    })();

    let lista = (function () {

    })();

    return {
        construct: construct,
    };

})();

let productosCarrito = {
    linkAgregarSelector: $(".carrito-agregar"),
    urlAgregarArticulo: '/',

    init: function (urlBase) {
        productosCarrito.urlAgregarArticulo = urlBase + 'ajaxAgregarArticulo.php';
        productosCarrito.linkAgregarSelector.on('click', productosCarrito.onClickAgregarCarrito);
    },

    onClickAgregarCarrito: function (event) {
        event.preventDefault();
        let eventTarget = $(event.target);
        //console.log(eventTarget.data('id'));

        let ajax = new Remote(productosCarrito.urlAgregarArticulo, 'GET', productosCarrito.onSuccessAjax);
        ajax.pushDataItem('idProducto', eventTarget.data('id'));
        ajax.request();
    },

    onSuccessAjax: function (data) {
        console.log(data);
        listaCarrito.agregarArticulo(data.payload);
    },
};

let listaCarrito = {
    contenedor: $('#carrito-lista'),

    agregarArticulo: function (payload) {
        listaCarrito.contenedor.append(payload);
    }
};

