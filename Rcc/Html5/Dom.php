<?php
/**
 * @Filename: Dom.php
 * @Description:
 * @CreatedAt: 22/06/19 8:51
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5;


interface Dom
{
    public function toHtml(): string;
}