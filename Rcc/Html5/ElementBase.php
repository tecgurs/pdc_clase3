<?php
/**
 * @Filename: ElementBase.php
 * @Description:
 * @CreatedAt: 22/06/19 8:53
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5;


/**
 * Class ElementBase
 * @package Rcc\Phalcon\Html5
 * @property string $htmlId
 */
abstract class ElementBase implements Element
{
    protected $htmlId;
    protected $classes = [];
    protected $properties = [];
    protected $elements = [];
    protected $hidden = false;

    public function append(Dom $dom)
    {
        $this->elements[] = $dom;
    }

    public function pushProperty(Property $property)
    {
        $this->properties[] = $property;
    }

    public function pushCustomData(CustomData $data)
    {
        $this->properties[] = $data->generateProperty();
    }

    public function show()
    {
        $this->hidden = false;
    }

    public function hide()
    {
        $this->hidden = true;
    }

    protected function getHtmlId(): string
    {
        if (empty($this->htmlId)) {
            return '';
        }

        return "id=\"{$this->htmlId}\"";
    }

    abstract protected function generateArrayClasses(): array;

    protected function getHtmlClasses(): string
    {
        $classes = $this->generateArrayClasses();
        if (empty($classes)) {
            return '';
        }

        return "class=\"{$this->arrayToHtmlClasses($classes)}\"";
    }

    protected function getHtmlElements(): string
    {
        if (count($this->elements) == 0) {
            return '';
        }

        $html = '';
        /** @var Dom $element */
        foreach ($this->elements as $element) {
            $html .= $element->toHtml();
        }

        return $html;
    }

    protected function getHtmlProperties(): string
    {
        if (empty($this->properties)) {
            return '';
        }

        $html = '';
        $cont = 0;
        /** @var Property $property */
        foreach ($this->properties as $property) {
            if ($cont) {
                $html .= ' '; // Glue
            }
            $html .= $property->toString();

            $cont++;
        }

        return $html;
    }

    private function arrayToHtmlClasses(array $classes): string
    {
        switch (count($classes)) {
            case 0: return '';
            case 1: return (string) array_shift($classes);
        }

        return implode(' ', $classes);
    }
}