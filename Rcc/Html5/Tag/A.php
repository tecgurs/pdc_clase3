<?php
/**
 * @Filename: A.php
 * @Description:
 * @CreatedAt: 22/06/19 10:19
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\Dom;
use Rcc\Html5\Property;

/**
 * Class A
 * @package Rcc\Html5\Tag
 */
class A extends Div
{
    protected $tagName = 'a';

    public function __construct(Dom $innerDom, string $href = '#', string $htmlId = '', array $classes = [])
    {
        parent::__construct($htmlId, $classes);
        $this->pushProperty(new Property('href', $href));
        $this->append($innerDom);
    }
}
