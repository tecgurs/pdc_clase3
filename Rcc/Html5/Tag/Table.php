<?php
/**
 * @Filename: Table.php
 * @Description:
 * @CreatedAt: 6/07/19 11:20
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Table extends Div
{
    protected $tagName = 'table';
}
