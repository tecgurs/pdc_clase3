<?php
/**
 * @Filename: Label.php
 * @Description:
 * @CreatedAt: 8/07/19 01:58 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\Property;

class Label extends Div
{
    protected $tagName = 'label';

    public function __construct(string $for, string $innerText = '', string $htmlId = '', array $classes = [])
    {
        parent::__construct($htmlId, $classes);
        $this->pushProperty(new Property('for', $for));
        if (!empty($innerText)) {
            $this->append(new Text($innerText));
        }
    }
}
