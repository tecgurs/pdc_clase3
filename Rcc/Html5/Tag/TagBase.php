<?php
/**
 * @Filename: TagBase.php
 * @Description:
 * @CreatedAt: 7/07/19 02:07 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\BootstrapElementBase;

abstract class TagBase extends BootstrapElementBase
{
    protected $tagName;
}
