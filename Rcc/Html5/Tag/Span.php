<?php
/**
 * @Filename: Span.php
 * @Description:
 * @CreatedAt: 27/06/19 13:07
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Span extends Div
{
    protected $tagName = 'span';

    public function __construct(string $htmlId = '', array $classes = [], string $innerText = '')
    {
        parent::__construct($htmlId, $classes);

        if (!empty($innerText)) {
            $this->append(new Text($innerText));
        }
    }
}
