<?php
/**
 * @Filename: Ul.php
 * @Description:
 * @CreatedAt: 22/06/19 10:14
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Ul extends Div
{
    protected $tagName = 'ul';
}
