<?php
/**
 * @Filename: H5.php
 * @Description:
 * @CreatedAt: 26/06/19 16:20
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class H5 extends Span
{
    protected $tagName = 'h5';
}
