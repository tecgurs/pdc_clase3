<?php
/**
 * @Filename: Text.php
 * @Description:
 * @CreatedAt: 22/06/19 10:50
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\Dom;

class Text implements Dom
{
    /** @var string  */
    private $text;

    public function __construct(string $text = '')
    {
        $this->text = htmlentities($text);
    }

    public function toHtml(): string
    {
        return $this->text;
    }
}
