<?php
/**
 * @Filename: Form.php
 * @Description:
 * @CreatedAt: 8/07/19 09:53 AM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Form extends Div
{
    protected $tagName = 'form';
}
