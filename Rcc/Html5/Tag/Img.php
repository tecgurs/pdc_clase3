<?php
/**
 * @Filename: Img.php
 * @Description:
 * @CreatedAt: 2/07/19 12:46
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\Property;

class Img extends Hr
{
    protected $tagName = 'img';

    public function __construct($src, $alt = '', string $htmlId = '', array $classes = [])
    {
        parent::__construct($htmlId, $classes);
        $this->pushProperty(new Property('src', $src));
        $this->pushProperty(new Property('alt', htmlentities($alt)));
    }
}
