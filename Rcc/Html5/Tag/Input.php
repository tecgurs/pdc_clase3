<?php
/**
 * @Filename: Input.php
 * @Description:
 * @CreatedAt: 4/07/19 01:21 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\Property;

class Input extends Hr
{
    protected $tagName = 'input';

    /** @var string */
    private $placeholder;
    /** @var string */
    private $value;

    public function __construct(string $name, string $type = 'text', array $classes = [], string $placeholder = '',
                                string $value = '')
    {
        $this->placeholder = $placeholder;
        $this->value = $value;
        parent::__construct($name, array_merge(['form-control'], $classes));
        $this->pushProperty(new Property('name', $name));
        $this->pushProperty(new Property('type', $type));
    }

    /**
     * @param string $placeholder
     * @return Input
     */
    public function setPlaceholder(string $placeholder): Input
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * @param string $value
     * @return Input
     */
    public function setValue(string $value): Input
    {
        $this->value = $value;

        return $this;
    }

    public function toHtml(): string
    {
        //$this->elements = [];
        //$this->properties = [];
        if (!empty($this->value)) {
            $this->pushProperty(new Property('value', $this->value));
        }
        if (!empty($this->placeholder)) {
            $this->pushProperty(new Property('placeholder', $this->placeholder));
        }

        return parent::toHtml();
    }
}
