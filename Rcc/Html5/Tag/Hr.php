<?php
/**
 * @Filename: Hr.php
 * @Description:
 * @CreatedAt: 4/07/19 12:33 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Hr extends TagBase
{
    protected $tagName = 'hr';

    public function __construct(string $htmlId, array $classes = [])
    {
        $this->htmlId = $htmlId;
        $this->classes = $classes;
    }

    public function toHtml(): string
    {
        return <<<html
<{$this->tagName} {$this->getHtmlId()} {$this->getHtmlClasses()} {$this->getHtmlProperties()}>
html;
    }
}
