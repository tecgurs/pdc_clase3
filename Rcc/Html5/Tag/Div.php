<?php
/**
 * @Filename: Div.php
 * @Description:
 * @CreatedAt: 22/06/19 9:22
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Div extends TagBase
{
    protected $tagName = 'div';

    /**
     * Div constructor.
     * @param string $htmlId
     * @param array $classes
     * @param bool $hidden
     */
    public function __construct(string $htmlId = '', array $classes = [], bool $hidden = false)
    {
        $this->htmlId = $htmlId;
        $this->classes = $classes;
        $this->hidden = $hidden;
    }

    public function toHtml(): string
    {
        return <<<html
<{$this->tagName} {$this->getHtmlId()} {$this->getHtmlClasses()} {$this->getHtmlProperties()}>
  {$this->getHtmlElements()}
</{$this->tagName}>
html;
    }
}
