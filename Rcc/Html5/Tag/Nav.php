<?php
/**
 * @Filename: Nav.php
 * @Description:
 * @CreatedAt: 27/06/19 12:05
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Nav extends Div
{
    protected $tagName = 'nav';
}
