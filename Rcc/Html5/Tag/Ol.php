<?php
/**
 * @Filename: Ol.php
 * @Description:
 * @CreatedAt: 2/07/19 12:34
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Ol extends Div
{
    protected $tagName = 'ol';
}
