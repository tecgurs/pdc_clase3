<?php
/**
 * @Filename: Button.php
 * @Description:
 * @CreatedAt: 26/06/19 18:19
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\Dom;
use Rcc\Html5\Property;

class Button extends Div
{
    protected $tagName = 'button';

    public function __construct(Dom $innerDom, string $htmlId = '', array $classes = [], bool $disabled = false)
    {
        parent::__construct($htmlId, array_merge(['btn'], $classes));
        $this->append($innerDom);
        $this->pushProperty(new Property('type', 'button'));

        if ($disabled) {
            $this->pushProperty(new Property('disabled'));
        }
    }
}
