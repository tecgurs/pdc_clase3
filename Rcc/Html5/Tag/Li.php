<?php
/**
 * @Filename: Li.php
 * @Description:
 * @CreatedAt: 22/06/19 10:16
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


use Rcc\Html5\Dom;

class Li extends Div
{
    protected $tagName = 'li';

    public function __construct(Dom $innerDom, string $htmlId = '', array $classes = [])
    {
        parent::__construct($htmlId, $classes);
        $this->append($innerDom);
    }

}
