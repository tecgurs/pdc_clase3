<?php
/**
 * @Filename: H3.php
 * @Description:
 * @CreatedAt: 2/07/19 13:00
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class H3 extends Span
{
    protected $tagName = 'h3';
}
