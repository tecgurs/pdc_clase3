<?php
/**
 * @Filename: Fieldset.php
 * @Description:
 * @CreatedAt: 8/07/19 10:10 AM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Fieldset extends Div
{
    protected $tagName = 'fieldset';
}
