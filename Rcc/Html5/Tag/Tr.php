<?php
/**
 * @Filename: Tr.php
 * @Description:
 * @CreatedAt: 6/07/19 11:25
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Tag;


class Tr extends Div
{
    protected $tagName = 'tr';
}
