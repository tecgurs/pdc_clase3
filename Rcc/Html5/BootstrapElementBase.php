<?php
/**
 * @Filename: BootstrapElementBase.php
 * @Description:
 * @CreatedAt: 22/06/19 9:15
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5;


abstract class BootstrapElementBase extends ElementBase
{
    protected function generateArrayClasses(): array
    {
        return $this->hidden ? array_merge($this->classes, ['d-none']) : $this->classes;
    }
}