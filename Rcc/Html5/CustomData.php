<?php
/**
 * @Filename: CustomData.php
 * @Description:
 * @CreatedAt: 22/06/19 9:45
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5;


/**
 * Class CustomData
 * @package Rcc\Phalcon\Html5
 * @property string $key
 * @property string $value
 */
class CustomData
{
    private $key;
    private $value;

    /**
     * CustomData constructor.
     * @param string $key
     * @param string $value
     */
    public function __construct(string $key, string $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function generateProperty(): Property
    {
        return new Property("data-{$this->key}", $this->value);
    }
}
