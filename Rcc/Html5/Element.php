<?php
/**
 * @Filename: Element.php
 * @Description:
 * @CreatedAt: 22/06/19 8:52
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5;


interface Element extends Dom
{
    public function append(Dom $dom);

    public function show();

    public function hide();
}