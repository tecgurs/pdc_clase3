<?php
/**
 * @Filename: Table.php
 * @Description:
 * @CreatedAt: 6/07/19 11:19
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Html5\Complex\Table\Tr;
use Rcc\Html5\Tag\Table as TabTable;

class Table extends TabTable
{
    public function appendTr(Tr $tr): Table
    {
        $this->append($tr);

        return $this;
    }
}
