<?php
/**
 * @Filename: Progress.php
 * @Description:
 * @CreatedAt: 26/06/19 16:11
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Html5\Property;
use Rcc\Html5\Tag\Div;
use Rcc\Html5\Tag\H5;

class Progress extends Div
{
    public function __construct(string $htmlId = '', bool $hidden = false)
    {
        parent::__construct($htmlId, ['row', 'my-3'], $hidden);
    }

    public function toHtml(): string
    {
        $this->appendInnerDom();

        return parent::toHtml();
    }

    private function appendInnerDom()
    {
        $this->append(new H5('', ['col-12', 'text-center'], 'Cargando...'));
        $this->append(new Div('', ['col-4']));
        $progress = new Div('', ['progress', 'col-4', 'p-0']);
        $progressBar = new Div(
            '',
            [
                'progress-bar',
                'progress-bar-striped',
                'progress-bar-animated',
                'w-100',
                'bg-secondary'
            ]
        );
        $progressBar->pushProperty(new Property('role', 'progressbar'));
        $progressBar->pushProperty(new Property('aria-valuenow', '100'));
        $progressBar->pushProperty(new Property('aria-valuemin', '0'));
        $progressBar->pushProperty(new Property('aria-valuemax', '100'));
        $progress->append($progressBar);
        $this->append($progress);
    }
}