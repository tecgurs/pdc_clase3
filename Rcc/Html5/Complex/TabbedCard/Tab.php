<?php
/**
 * @Filename: Tab.php
 * @Description:
 * @CreatedAt: 6/07/19 9:38
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\TabbedCard;


use Rcc\Html5\CustomData;
use Rcc\Html5\Dom;
use Rcc\Html5\Property;
use Rcc\Html5\Tag\A;
use Rcc\Html5\Tag\Div;
use Rcc\Html5\Tag\Li;
use Rcc\Html5\Tag\Text;

class Tab
{
    /** @var string */
    private $name;
    /** @var string */
    private $parentName;
    /** @var string */
    private $caption;
    /** @var Dom */
    private $contentDom;

    private $isActive = false;

    /**
     * Tab constructor.
     * @param string $name
     * @param string $parentName
     * @param string $caption
     * @param Dom $contentDom
     */
    public function __construct(string $name, string $parentName, string $caption, Dom $contentDom)
    {
        $this->name = $name;
        $this->parentName = $parentName;
        $this->caption = $caption;
        $this->contentDom = $contentDom;
    }

    public function activateOn(): Tab
    {
        $this->isActive = true;

        return $this;
    }

    /**
     * @return Li
     */
    public function generateNav(): Li
    {
        $aClasses = ['nav-link'];
        if ($this->isActive) {
            $aClasses = array_merge($aClasses, ['active']);
        }
        $a = new A(
            new Text($this->caption),
            "#{$this->parentName}-{$this->name}",
            "{$this->parentName}-tab-{$this->name}",
            $aClasses
        );
        $a->pushCustomData(new CustomData('toggle', 'tab'));
        $a->pushProperty(new Property('role', 'tab'));
        $a->pushProperty(new Property('aria-controls', "{$this->parentName}-{$this->name}"));
        $a->pushProperty(new Property('aria-selected', $this->isActive));

        return new Li($a, '', ['nav-item']);
    }

    /**
     * @return Div
     */
    public function generatePane(): Div
    {
        $classes = ['tab-pane'];
        if ($this->isActive) {
            $classes = array_merge($classes, ['show', 'active']);
        }

        $pane = new Div("{$this->parentName}-{$this->name}", $classes);
        $pane->pushProperty(new Property('role', 'tabpanel'));
        $pane->pushProperty(new Property('aria-labelledby', "{$this->parentName}-tab-{$this->name}"));
        $pane->append($this->contentDom);

        return $pane;
    }
}
