<?php
/**
 * @Filename: Form.php
 * @Description:
 * @CreatedAt: 8/07/19 09:57 AM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Html5\Complex\Form\Row;
use Rcc\Html5\Property;
use Rcc\Html5\Tag\Form as TagForm;

class Form extends TagForm
{
    public function __construct(string $htmlId, array $classes = [])
    {
        parent::__construct($htmlId, $classes);
        $this->pushProperty(new Property('novalidate'));
    }

    public function appendRow(Row $row)
    {
        parent::append($row);
    }
}
