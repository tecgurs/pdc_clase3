<?php
/**
 * @Filename: HeaderButton.php
 * @Description:
 * @CreatedAt: 2/07/19 13:53
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Card;


use Rcc\Phalcon\Html5\Complex\Iconic;
use Rcc\Phalcon\Html5\Property;
use Rcc\Phalcon\Html5\Tag\Button;

class HeaderButton extends Button
{
    public function __construct(string $icon, string $title, string $color = 'secondary', array $classes = [], string $htmlId = '')
    {
        $classes = array_merge(['ml-1', 'p-0'], $classes);
        parent::__construct(new Iconic($icon, $color), $htmlId, $classes);
        $this->pushProperty(new Property('title', $title));
    }
}
