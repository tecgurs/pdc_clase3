<?php
/**
 * @Filename: Iconic.php
 * @Description:
 * @CreatedAt: 27/06/19 17:44
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Html5\Tag\Span;

class Iconic extends Span
{
    public function __construct(string $name, string $color = '', array $classes = [])
    {
        $classes = array_merge(['oi', "oi-{$name}"], $classes);
        if (!empty($color)) {
            $classes[] = "text-{$color}";
        }

        parent::__construct('', $classes);
    }
}
