<?php
/**
 * @Filename: Accordion.php
 * @Description:
 * @CreatedAt: 22/06/19 9:30
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Phalcon\Html5\Complex\Accordion\FirstLevel;
use Rcc\Phalcon\Html5\Tag\Div;

class Accordion extends Div
{
    //private $categories = [];

    public function __construct(string $htmlId = '')
    {
        parent::__construct($htmlId, ['mt-1']);
    }

    public function pushFirstLevel(FirstLevel $category)
    {
        $this->append($category);
    }
}
