<?php
/**
 * @Filename: Modal.php
 * @Description:
 * @CreatedAt: 6/07/19 07:12 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Html5\Complex\Modal\Button;
use Rcc\Html5\Dom;
use Rcc\Html5\Property;
use Rcc\Html5\Tag\Div;
use Rcc\Html5\Tag\H5;

class Modal extends Div
{
    const SIZE_SMALL = 'modal-sm';
    const SIZE_LARGE = 'modal-lg';
    const SIZE_XLARGE = 'modal-xl';

    /** @var string */
    private $name;
    /** @var Dom */
    private $titleDom;
    /** @var Dom */
    private $bodyDom;
    /** @var bool */
    private $footerEnabled;
    /** @var string */
    private $size;

    /** @var Button */
    private $buttonCancel;
    /** @var Button */
    private $buttonOk;

    /**
     * Modal constructor.
     * @param string $name
     * @param Dom $titleDom
     * @param Dom $bodyDom
     * @param bool $footerEnabled
     * @param string $size
     */
    public function __construct(string $name, Dom $titleDom, Dom $bodyDom, bool $footerEnabled = true, string $size = '')
    {
        parent::__construct($name, ['modal', 'fade']);
        $this->pushProperty(new Property('tabindex', '-1'));
        $this->pushProperty(new Property('role', 'dialog'));
        $this->pushProperty(new Property('aria-labelledby', "{$name}-title"));
        $this->pushProperty(new Property('aria-hidden', 'true'));
        $this->name = $name;
        $this->titleDom = $titleDom;
        $this->bodyDom = $bodyDom;
        $this->footerEnabled = $footerEnabled;
        $this->size = $size;

        $this->buttonCancel = Button::defaultCancel();
        $this->buttonOk = Button::defaultOk($name);
    }

    public function disableFooter(): Modal
    {
        $this->footerEnabled = false;

        return $this;
    }

    public function replaceBody(Dom $bodyDom): Modal
    {
        $this->bodyDom = $bodyDom;

        return $this;
    }

    public function replaceButtonOk(Button $buttonOk): Modal
    {
        $this->buttonOk = $buttonOk;

        return $this;
    }

    public function toHtml(): string
    {
        $this->append($this->generateModalDialog());
        return parent::toHtml();
    }

    private function generateModalDialog(): Div
    {
        $classes = ['modal-dialog'];
        if (!empty($this->size)) {
            $classes = array_merge($classes, [$this->size]);
        }
        $modalDialog = new Div('', $classes);
        $modalDialog->pushProperty(new Property('role', 'document'));
        $modalDialog->append($this->generateModalContent());

        return $modalDialog;
    }

    private function generateModalContent(): Div
    {
        $modalContent = new Div('', ['modal-content']);
        $modalContent->append($this->generateModalHeader());
        $modalContent->append($this->generateModalBody());
        $modalContent->append($this->generateModalFooter());

        return $modalContent;
    }

    private function generateModalHeader(): Div
    {
        $header = new Div('', ['modal-header']);

        $h5 = new H5("{$this->name}-title", ['modal-title']);
        $h5->append($this->titleDom);
        $header->append($h5);

        $header->append(Button::defaultClose());

        return $header;
    }

    private function generateModalBody(): Div
    {
        $body = new Div("{$this->name}-body", ['modal-body']);
        $body->append($this->generateModalError());
        $body->append($this->bodyDom);

        return $body;
    }

    private function generateModalFooter(): Div
    {
        $footer = new Div("{$this->name}-footer", ['modal-footer']);
        $footer->append($this->buttonCancel);
        $footer->append($this->buttonOk);

        return $footer;
    }

    private function generateModalError(): Div
    {
        return new Div("{$this->name}-error", ['alert', 'alert-danger'], true);
    }
}
