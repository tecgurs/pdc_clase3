<?php
/**
 * @Filename: Tr.php
 * @Description:
 * @CreatedAt: 6/07/19 11:28
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Table;


use Rcc\Html5\Tag\Tr as TagTr;

class Tr extends TagTr
{
    public function appendCell(TableCell $cell): Tr
    {
        $this->append($cell);

        return $this;
    }
}
