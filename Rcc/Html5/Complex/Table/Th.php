<?php
/**
 * @Filename: Th.php
 * @Description:
 * @CreatedAt: 6/07/19 11:30
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Table;


use Rcc\Html5\Tag\Th as TagTh;

class Th extends TagTh implements TableCell
{

}
