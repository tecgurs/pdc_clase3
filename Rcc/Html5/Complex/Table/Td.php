<?php
/**
 * @Filename: Td.php
 * @Description:
 * @CreatedAt: 6/07/19 11:29
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Table;


use Rcc\Html5\Tag\Td as TagTd;

class Td extends TagTd implements TableCell
{

}
