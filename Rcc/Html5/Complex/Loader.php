<?php
/**
 * @Filename: Loader.php
 * @Description:
 * @CreatedAt: 26/06/19 15:57
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Phalcon\Html5\Tag\Div;
use Rcc\Phalcon\Html5\Tag\H5;

class Loader extends Div
{
    /** @var string */
    private $name;

    public function __construct(string $name, array $classes = [], $hidden = false)
    {
        $this->name = $name;
        parent::__construct("{$name}-loader", $classes, $hidden);
    }

    public function toHtml(): string
    {
        $this->appendInnerDom();

        return parent::toHtml();
    }

    private function appendInnerDom()
    {
        $this->append(new Progress("{$this->name}-loading"));
        $this->append($this->generateEmptyJumbotron());
        $this->append(new Div("{$this->name}-header", ['row', 'mb-3']));
        $this->append(new Div("{$this->name}-body", ['row', 'mb-3']));
        $this->append(new Div("{$this->name}-footer", ['row', 'mb-3']));
    }

    private function generateEmptyJumbotron(): Div
    {
        $div = new Div("{$this->name}-empty", ['jumbotron', 'm-3']);
        $div->append(new H5('', [], 'No hay resultados'));
        $div->hide();

        return $div;
    }
}
