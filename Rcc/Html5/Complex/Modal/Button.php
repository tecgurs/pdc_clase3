<?php
/**
 * @Filename: Button.php
 * @Description:
 * @CreatedAt: 7/07/19 01:29 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Modal;


use Rcc\Html5\Complex\Iconic;
use Rcc\Html5\CustomData;
use Rcc\Html5\Property;
use Rcc\Html5\Tag\Button as TagButton;
use Rcc\Html5\Tag\Text;

class Button extends TagButton
{
    public static function defaultClose(): Button
    {
        $iconic = new Iconic('x');
        $iconic->pushProperty(new Property('aria-hidden', 'true'));
        $button = new self($iconic);
        $button->classes = ['btn', 'btn-sm', 'text-muted']; //Para sobreescribir la clase 'btn' que por default tiene TagButton
        $button->configureDismissButton();
        $button->pushProperty(new Property('aria-label', 'Close'));

        return $button;
    }

    public static function defaultCancel(): Button
    {
        $button = new self(new Iconic('action-undo'), '', ['btn-outline-danger']);
        $button->append(new Text(' Cancelar'));
        $button->configureDismissButton();

        return $button;
    }

    public static function defaultOk(string $modalName, string $textAfterIconic = 'Aceptar'): Button
    {
        $button = new self(new Iconic('check'), "{$modalName}-ok", ['btn-outline-success'], true);
        $button->append(new Text(" {$textAfterIconic}"));

        return $button;
    }

    private function configureDismissButton()
    {
        $this->pushCustomData(new CustomData('dismiss', 'modal'));
    }
}
