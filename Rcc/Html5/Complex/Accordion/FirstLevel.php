<?php
/**
 * @Filename: FirstLevel.php
 * @Description:
 * @CreatedAt: 22/06/19 10:28
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Accordion;


use Rcc\Phalcon\Html5\CustomData;
use Rcc\Phalcon\Html5\Dom;
use Rcc\Phalcon\Html5\Property;
use Rcc\Phalcon\Html5\Tag\A;
use Rcc\Phalcon\Html5\Tag\Div;
use Rcc\Phalcon\Html5\Tag\Span;
use Rcc\Phalcon\Html5\Tag\Text;
use Rcc\Phalcon\Html5\Tag\Ul;

/**
 * Class FirstLevel
 * @package Rcc\Phalcon\Html5\Complex\Accordion
 * @property Dom $caption
 * @property string $parentId
 */
class FirstLevel extends Div
{
    private $caption;
    private $parentId;
    private $items = [];

    public function __construct(string $htmlId, Dom $caption, string $parentId)
    {
        parent::__construct($htmlId, ['card']);
        $this->caption = $caption;
        $this->parentId = $parentId;
    }

    public function pushSecondLevel(SecondLevel $item)
    {
        $this->items[] = $item;
    }

    public function toHtml(): string
    {
        $this->elements = [];
        $this->append($this->generateHeaderDom());
        $this->append($this->generateBodyDom());

        return parent::toHtml();
    }

    private function generateHeaderDom(): Dom
    {
        $classes = ['card-header', 'btn', 'text-left', 'py-1', 'text-light', 'blue-background', 'orange-border'];
        if (empty($this->items)) {
            $span = new Span('', $classes);
            $span->append($this->caption);
            return $span;
        }
        if (count($this->items) == 1) {
            /** @var SecondLevel $item */
            $item = $this->items[0];
            $item->renderAsFirstLevel();

            return $item;
        }
        $classes[] = 'font-weight-bold';
        $headerDom = new Div($this->getHeaderId(), $classes);
        $headerDom->append($this->caption);
        $headerDom->pushProperty(new Property('aria-expanded', 'false'));
        $headerDom->pushProperty(new Property('aria-controls', $this->getBodyId()));
        $headerDom->pushCustomData(new CustomData('toggle', 'collapse'));
        $headerDom->pushCustomData(new CustomData('target', "#{$this->getBodyId()}"));

        return $headerDom;
    }

    private function generateBodyDom(): Dom
    {
        if (count($this->items) < 2) {
            return new Text();
        }
        $bodyUl = new Ul($this->getBodyId(), ['card-body', 'list-group', 'list-group-flush', 'collapse', 'p-0', 'bg-secondary', 'text-light']);
        $bodyUl->pushProperty(new Property('aria-labelledby', $this->getHeaderId()));
        $bodyUl->pushCustomData(new CustomData('parent', "#{$this->parentId}"));

        foreach ($this->items as $item) {
            $bodyUl->append($item);
        }

        return $bodyUl;
    }

    private function getHeaderId(): string
    {
        return "heading-{$this->htmlId}";
    }

    private function getBodyId(): string
    {
        return "collapse-{$this->htmlId}";
    }
}