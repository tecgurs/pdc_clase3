<?php
/**
 * @Filename: Factory.php
 * @Description:
 * @CreatedAt: 22/06/19 12:02
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Accordion;


use Rcc\Html5\Complex\Accordion;
use Rcc\Html5\Tag\Text;

abstract class Factory
{
    public static function getCategorias(): Accordion
    {
        $htmlId = 'accordion-categorias';
        $accordion = new Accordion($htmlId);

        $cat1 = new FirstLevel('cementos', new Text('Cementos'), $htmlId);
        $item1 = new SecondLevel('cal', new Text('Cal'));
        $cat1->pushSecondLevel($item1);
        $item2 = new SecondLevel('cemento', new Text('Cemento'));
        $cat1->pushSecondLevel($item2);
        $accordion->pushFirstLevel($cat1);

        $cat2 = new FirstLevel('cat2', new Text('Categoria 2'), $htmlId);
        $item21 = new SecondLevel('item21', new Text('Item 21'));
        $cat2->pushSecondLevel($item21);
        $item22 = new SecondLevel('item22', new Text('Item 22'));
        $cat2->pushSecondLevel($item22);
        $item23 = new SecondLevel('item23', new Text('Item 23'));
        $cat2->pushSecondLevel($item23);
        $accordion->pushFirstLevel($cat2);

        $cat5 = new FirstLevel('aditivos', new Text('Aditivos'), $htmlId);
        $accordion->pushFirstLevel($cat5);

        return $accordion;
    }
}