<?php
/**
 * @Filename: SecondLevel.php
 * @Description:
 * @CreatedAt: 22/06/19 10:28
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Accordion;


use Rcc\Phalcon\Html5\Dom;
use Rcc\Phalcon\Html5\Tag\A;
use Rcc\Phalcon\Html5\Tag\Button;

class SecondLevel extends Button
{
    private $renderAsFirstLevel = false;

    public function __construct(string $htmlId, Dom $caption, array $classes = [])
    {
        //TODO change params order to delete this overwritted __construct
        parent::__construct($caption, $htmlId, $classes);
    }

    public function renderAsFirstLevel()
    {
        $this->renderAsFirstLevel = true;
    }

    public function toHtml(): string
    {
        $specificClasses = ['list-group-item-action', 'py-0', 'pl-2', 'blue-background-transparent'];
        if ($this->renderAsFirstLevel) {
            $specificClasses = ['card-header', 'list-group-item-action', 'text-left', 'py-1', 'text-light', 'blue-background', 'orange-border', 'font-weight-bold'];
        }
        $this->classes = array_merge($this->classes, $specificClasses);
        return parent::toHtml();
    }

    /*public function renderAsFirstLevel(): string
    {
        $this->classes = array_merge($this->classes, ['py-0', 'pl-2', 'blue-background-transparent']);
        return parent::toHtml();
    }*/
}

