<?php
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 27/06/19 12:56
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Pagination;


use Rcc\Phalcon\Html5\CustomData;
use Rcc\Phalcon\Html5\Dom;
use Rcc\Phalcon\Html5\Tag\A;
use Rcc\Phalcon\Html5\Tag\Li;
use Rcc\Phalcon\Html5\Tag\Span;
use Rcc\Phalcon\Html5\Tag\Text;

class Item extends Li
{
    /** @var int */
    private $pageNumber;
    /** @var Dom */
    private $captionDom;
    /** @var string  */
    private $buttonHtmlClass;
    /** @var bool  */
    private $active;

    public function __construct(int $pageNumber, Dom $captionDom, string $buttonHtmlClass, $active = false)
    {
        $this->pageNumber = $pageNumber;
        $this->captionDom = $captionDom;
        $this->buttonHtmlClass = $buttonHtmlClass;
        $this->active = $active;
        $classes = ['page-item'];
        if ($active) $classes[] = 'active';
        parent::__construct($this->generateInnerDom(), '', $classes);
    }

    private function generateInnerDom(): Dom
    {
        if ($this->active) {
            $dom = new Span('', ['page-link', 'p-1']);
            $dom->append($this->captionDom);
            return $dom;
        } else {
            $dom = new A($this->captionDom, '#', '', ['page-link', 'p-1', $this->buttonHtmlClass]);
            $dom->pushCustomData(new CustomData('page', $this->pageNumber));
            return $dom;
        }
    }
}