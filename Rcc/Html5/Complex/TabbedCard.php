<?php
/**
 * @Filename: TabbedCard.php
 * @Description:
 * @CreatedAt: 6/07/19 9:18
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Html5\Complex\TabbedCard\Tab;
use Rcc\Html5\Dom;
use Rcc\Html5\Property;
use Rcc\Html5\Tag\Div;
use Rcc\Html5\Tag\Ul;

class TabbedCard extends Div
{
    /** @var string */
    private $name;
    /** @var Dom */
    private $titleDom;
    /** @var array  */
    private $tabs = [];

    public function __construct(string $name, Dom $titleDom, array $classes = [])
    {
        $this->name = $name;
        $this->titleDom = $titleDom;
        $classes = array_merge(['card', 'shadow'], $classes);
        parent::__construct($name . '-tabbed', $classes);
    }

    public function pushTab(Tab $tab)
    {
        $this->tabs[] = $tab;
    }

    public function toHtml(): string
    {
        if (count($this->tabs) == 0) {
            return 'ERROR!! Esta TabbedCard no tiene tabs. No se puede renderizar';
        }

        /** @var Tab $firstTab */ //Para activar la primera tab
        $firstTab = $this->tabs[0];
        $firstTab->activateOn();

        $this->append($this->generateHeader());
        $this->append($this->generateBody());

        return parent::toHtml();
    }

    private function generateHeader(): Div
    {
        $header = new Div('', ['card-header']);
        $ul = new Ul("{$this->name}-navs", ['nav', 'nav-tabs', 'card-header-tabs']);
        $ul->pushProperty(new Property('role', 'tablist'));

        /** @var Tab $tab */
        foreach ($this->tabs as $tab) {
            $ul->append($tab->generateNav());
        }

        $header->append($ul);

        return $header;
    }

    private function generateBody(): Div
    {
        $body = new Div('', ['card-body']);
        $body->append($this->titleDom);
        $panels = new Div("{$this->name}-panels", ['tab-content']);

        /** @var Tab $tab */
        foreach ($this->tabs as $tab) {
            $panels->append($tab->generatePane());
        }

        $body->append($panels);
        return $body;
    }
}
