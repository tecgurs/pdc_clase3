<?php
/**
 * @Filename: Card.php
 * @Description:
 * @CreatedAt: 2/07/19 13:47
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Phalcon\Html5\Complex\Card\HeaderButton;
use Rcc\Phalcon\Html5\Dom;
use Rcc\Phalcon\Html5\Tag\Div;
use Rcc\Phalcon\Html5\Tag\Span;
use Rcc\Phalcon\Html5\Tag\Ul;

class Card extends Div
{
    /** @var Dom */
    private $title;
    /** @var Dom */
    private $body;
    private $headerButtons = [];

    public function __construct(Dom $title, Dom $body, array $classes = [], string $htmlId = '', bool $hidden = false)
    {

        parent::__construct($htmlId, array_merge(['card', 'shadow'], $classes), $hidden);
        $this->title = $title;
        $this->body = $body;
    }

    public function pushHeaderButton(HeaderButton $button)
    {
        $this->headerButtons[] = $button;
    }

    public function toHtml(): string
    {
        $this->elements = []; // Prevent for many toHtml() calls
        $this->append($this->generateHeaderDiv());
        $this->append($this->generateBodyDiv());

        return parent::toHtml();
    }

    private function generateHeaderDiv(): Div
    {
        $classes = ['card-header', 'py-1'];
        if (count($this->headerButtons) == 0) {
            $headerDiv = new Div('', $classes);
            $headerDiv->append($this->generateTitleSpan());

            return $headerDiv;
        }

        $headerDiv = new Div('', array_merge($classes, ['navbar']));
        $headerDiv->append($this->generateTitleSpan());
        $headerDiv->append($this->generateHeaderRightMenu());

        return $headerDiv;
    }

    private function generateBodyDiv(): Div
    {
        $div = new Div('', ['card-body', 'py-1']);
        $div->append($this->body);

        return $div;
    }

    private function generateHeaderRightMenu(): Ul
    {
        $menu = new Ul('', ['nav', 'ml-auto']);
        /** @var HeaderButton $button */
        foreach ($this->headerButtons as $button) {
            $menu->append($button);
        }

        return $menu;
    }

    private function generateTitleSpan(): Span
    {
        $span = new Span('', ['font-weight-bold']);
        $span->append($this->title);

        return $span;
    }
}
