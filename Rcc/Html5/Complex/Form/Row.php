<?php
/**
 * @Filename: Row.php
 * @Description:
 * @CreatedAt: 8/07/19 10:01 AM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Form;


use Rcc\Html5\Tag\Div;

class Row extends Div
{
    public function __construct(string $htmlId, array $classes = [], bool $hidden = false)
    {
        parent::__construct($htmlId, array_merge(['form-row'], $classes), $hidden);
    }

    public function appendFielset(Fieldset $fieldset)
    {
        $this->append($fieldset);
    }
}
