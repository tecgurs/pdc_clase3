<?php
/**
 * @Filename: InputBase.php
 * @Description:
 * @CreatedAt: 8/07/19 08:07 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Form;


abstract class InputBase extends FieldsetBase
{
    protected $type = 'text';
}
