<?php
/**
 * @Filename: Password.php
 * @Description:
 * @CreatedAt: 8/07/19 08:35 PM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Form;


class Password extends Text
{
    protected $type = 'password';
}
