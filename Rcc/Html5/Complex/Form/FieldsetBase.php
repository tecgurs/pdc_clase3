<?php
/**
 * @Filename: FieldsetBase.php
 * @Description:
 * @CreatedAt: 8/07/19 10:08 AM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Form;


use Rcc\Html5\Property;
use Rcc\Html5\Tag\Fieldset as TagFieldset;

abstract class FieldsetBase extends TagFieldset implements Fieldset
{
    /** @var bool */
    protected $disabled;

    public function __construct(string $htmlId = '', array $classes = [], bool $disabled = false)
    {
        parent::__construct($htmlId, array_merge(['form-group'], $classes));
        $this->disabled = $disabled;
    }

    public function disable()
    {
        $this->disabled = true;
    }

    public function toHtml(): string
    {
        if ($this->disabled) {
            $this->pushProperty(new Property('disabled'));
        }
        return parent::toHtml();
    }
}
