<?php
/**
 * @Filename: Fieldset.php
 * @Description:
 * @CreatedAt: 8/07/19 10:07 AM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Form;


use Rcc\Html5\Dom;

interface Fieldset extends Dom
{
    public function disable();
}
