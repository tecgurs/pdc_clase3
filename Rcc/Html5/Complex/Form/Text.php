<?php
/**
 * @Filename: Text.php
 * @Description:
 * @CreatedAt: 8/07/19 10:25 AM
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex\Form;


use Rcc\Html5\Dom;
use Rcc\Html5\Element;
use Rcc\Html5\Tag\Div;
use Rcc\Html5\Tag\Input;
use Rcc\Html5\Tag\Label;
use Rcc\Html5\Tag\Span;
use Rcc\Html5\Tag\Text as TagText;

class Text extends InputBase
{
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $placeholder;
    /** @var string  */
    protected $label;
    /** @var string  */
    protected $feedback;
    /** @var Dom */
    protected $prefixDom;

    public function __construct(string $name, array $classes = [], bool $disabled = false, string $placeholder = '',
                                string $label = '', string $feedback= 'Campo obligatorio', Dom $prefixDom = null)
    {
        $this->name = $name;
        $this->placeholder = $placeholder;
        $this->label = $label;
        $this->feedback = $feedback;
        if (!empty($prefixDom)) {
            $this->prefixDom = $prefixDom;
        }
        parent::__construct("{$name}-fieldset", $classes, $disabled);

    }

    /**
     * @param string $placeholder
     * @return Text
     */
    public function setPlaceholder(string $placeholder): Text
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param string $label
     * @return Text
     */
    public function setLabel(string $label): Text
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param string $feedback
     * @return Text
     */
    public function setFeedback(string $feedback): Text
    {
        $this->feedback = $feedback;
        return $this;
    }

    /**
     * @param Dom $prefixDom
     * @return Text
     */
    public function setPrefixDom(Dom $prefixDom): Text
    {
        $this->prefixDom = $prefixDom;
        return $this;
    }

    public function toHtml(): string
    {
        //$this->elements = [];
        if (empty(!$this->label)) {
            $this->append(new Label($this->name, $this->label));
        }
        if (empty($this->prefixDom)) {
            $this->appendInputAndFeedback();
        } else {
            $this->append($this->generateInputGroup());
        }

        return parent::toHtml();
    }

    protected function appendInputAndFeedback(Element $parent = null): Element
    {
        $parent = empty($parent) ? $this : $parent;
        $parent->append($this->generateInput());
        if (!empty($this->feedback)) {
            $parent->append($this->generateFeedbackDom());
        }

        return $parent;
    }

    protected function generateInput(): Input
    {
        $input = new Input($this->name, $this->type);
        if (!empty($this->placeholder)) {
            $input->setPlaceholder($this->placeholder);
        }

        return $input;
    }

    protected function generateFeedbackDom(): Dom
    {
        $feedback = new Div("{$this->name}-feedback", ['invalid-feedback']);
        $feedback->append(new TagText($this->feedback));

        return $feedback;
    }

    protected function generateInputGroup(): Div
    {
        $inputGroup = new Div("{$this->name}-input-group", ['input-group']);
        $inputGroup->append($this->generateInputGroupPrepend());
        $this->appendInputAndFeedback($inputGroup);

        return $inputGroup;
    }

    protected function generateInputGroupPrepend(): Div
    {
        $inputGroupPrepend = new Div("{$this->name}-input-group-prepend", ['input-group-prepend']);
        $inputGroupPrepend->append($this->generateInputGroupText());

        return $inputGroupPrepend;
    }

    protected function generateInputGroupText(): Span
    {
        $inputGroupText = new Span("{$this->name}-input-group-text", ['input-group-text']);
        $inputGroupText->append($this->prefixDom);

        return $inputGroupText;
    }
}
