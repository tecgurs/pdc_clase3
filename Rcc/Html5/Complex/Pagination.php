<?php
/**
 * @Filename: Pagination.php
 * @Description:
 * @CreatedAt: 27/06/19 12:04
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5\Complex;


use Rcc\Html5\Complex\Pagination\Item;
use Rcc\Html5\Tag\Nav;
use Rcc\Html5\Tag\Text;
use Rcc\Html5\Tag\Ul;

class Pagination extends Nav
{
    /** @var string */
    private $buttonsHtmlClass;
    /** @var int */
    private $totalPages;
    /** @var int */
    private $currentPage;
    /** @var int */
    private $maxDisplay;

    public function __construct(string $buttonsHtmlClass, int $totalPages, int $currentPage = 1,  array $classes = [],
                                int $maxDisplay = 11)
    {
        // Se forza a que maxDisplay no sea menor a 11.
        $this->maxDisplay = $maxDisplay < 11 ? 11 : $maxDisplay;
        parent::__construct('', $classes);
        $this->buttonsHtmlClass = $buttonsHtmlClass;
        // Se forza a que totalPages no sea cero ni negativo,
        $this->totalPages = $totalPages < 1 ? 1 : $totalPages;
        // Se forza a currentPage no sea cero ni negativo, además se forza a que currentPage no sea mayor a totalPages
        $this->currentPage = $currentPage < 1 ? 1 : $currentPage > $this->totalPages ? $this->totalPages : $currentPage;

    }

    public function toHtml(): string
    {
        $this->append($this->generateUl());

        return parent::toHtml();
    }

    private function generateUl(): Ul
    {
        $ul = new Ul('',['pagination', /*'row',*/ 'my-1']);
        $pages = $this->generatePagesArray();
        foreach ($pages as $page) {
            $ul->append($page);
        }

        return $ul;
    }

    private function generatePagesArray(): array
    {
        if ($this->totalPages > $this->maxDisplay) {
            return $this->generatePartial();
        }

        return $this->generateFull();
    }

    private function generatePartial(): array
    {
        $middle = $this->calculateMiddle();
        if ($this->currentPage < $middle) {
            return $this->generateLeft();
        }
        if ($this->currentPage > $this->totalPages + $middle - $this->maxDisplay) {
            return $this->generateRight();
        }
        return $this->generateCenter();
    }

    private function generateFull(): array
    {
        $pages = [];
        for ($cont = 1; $cont <= $this->totalPages; $cont++) {
            $pages[] = new Item(
                $cont,
                new Text($cont),
                $this->buttonsHtmlClass,
                $cont == $this->currentPage
            );
        }

        return $pages;
    }

    private function generateLeft(): array
    {
        $pages = [];
        for ($cont = 1; $cont <= $this->maxDisplay - 1; $cont++) {
            $pages[] = new Item(
                $cont,
                new Text($cont),
                $this->buttonsHtmlClass,
                $cont == $this->currentPage
            );
        }
        $pages[] = $this->generateLast();

        return $pages;
    }

    private function generateCenter(): array
    {
        $first = [$this->generateFirst()];

        //$middle = (int) ceil($this->maxDisplay / 2);
        $center = $this->generateNumbered(
            $this->currentPage - $this->calculateMiddle() + 1,
            $this->maxDisplay-2
        );

        $last = [$this->generateLast()];

        return array_merge($first, $center, $last);
    }

    private function generateRight(): array
    {
        $pages = [$this->generateFirst()];

        return array_merge($pages, $this->generateNumbered(
            $this->totalPages + 1 - $this->maxDisplay,
            $this->maxDisplay - 1
        ));
    }

    private function generateFirst(): Item
    {
        return new Item(1, new Iconic('media-skip-backward'), $this->buttonsHtmlClass);
    }

    private function generateLast(): Item
    {
        return new Item($this->totalPages, new Iconic('media-skip-forward'), $this->buttonsHtmlClass);
    }

    private function generateNumbered(int $offset, int $items): array
    {
        $pages = [];

        for ($cont = 1; $cont <= $items; $cont++) {
            $pageNumber = $cont + $offset;
            $pages[] = new Item(
                $pageNumber,
                new Text($pageNumber),
                $this->buttonsHtmlClass,
                $pageNumber == $this->currentPage
            );
        }

        return $pages;
    }

    public function calculateMiddle(): int
    {
        return (int) ceil($this->maxDisplay / 2);
    }
}