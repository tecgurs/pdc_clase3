<?php
/**
 * @Filename: Property.php
 * @Description:
 * @CreatedAt: 22/06/19 9:48
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Rcc\Html5;


/**
 * Class Property
 * @package Rcc\Phalcon\Html5
 * @property string $name
 * @property string $value
 */
class Property
{
    private $name;
    private $value;

    /**
     * Property constructor.
     * @param string $name
     * @param string $value
     */
    public function __construct(string $name, string $value = '')
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function toString(): string
    {
        return $this->name . $this->getStringValue();
    }

    private function getStringValue(): string
    {
        if (empty($this->value)) {
            return '';
        }
        return "=\"{$this->value}\"";
    }
}