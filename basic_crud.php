<?php

use Tg\BasicCrud\ValidatorFactory;

require_once 'autoload.php';

if (!empty($_POST)) {

  $validator = ValidatorFactory::create();
  if ($validator->validate()) {
    echo "formulario validado"; exit;
  } else {
    echo $validator->getMessage(); exit;
  }
  var_dump($validator); exit;

  var_dump($_POST); exit;

}

//$errorMessage = 'este es un mensaje de error';



// View or Template
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Basic CRUD">
    <meta name="keywords" content="Basic CRUD, PHP, JS, Materialize">

    <title>Basic CRUD</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection">

</head>
<body>

<div class="container" role="main">
  <div class="row">
    <div class="col s12">
      <div class="card center">
        <div class="card-title">Create</div>
      </div>
    </div>
    <?php if (isset($errorMessage)) { ?>
    <div class="col s12">
      <div class="card center">
        <div class="card-title red-text"><?php echo $errorMessage ?></div>
      </div>
    </div>
    <?php } ?>
    <form class="col s12" action="#" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="input-field col s12 m6">
          <input id="nombre" name="nombre" type="text">
          <label for="nombre">Nombre</label>
        </div>
        <div class="input-field col s12 m6">
          <input id="precio" name="precio" type="text">
          <label for="precio">Precio</label>
        </div>
        <div class="input-field col s12 m6" >
          <select id="unidad" name="unidad">
            <option value="kilo">Kilo</option>
            <option value="bolsa">Bolsa</option>
          </select>
          <label for="unidad"></label>
        </div>
        <div class="file-field input-field col s12 m6">
          <div class="btn blue-grey">
            <span>Archivo</span>
            <input id="foto" name="foto" type="file">
          </div>
          <div class="file-path-wrapper">
            <!--suppress HtmlFormInputWithoutLabel -->
            <input class="file-path" type="text">
          </div>
        </div>
        <div class="col s12">
          <button class="btn blue-grey" type="submit">Crear</button>
        </div>
      </div>
    </form>
  </div>
</div>

<!--  Scripts-->
<script src="js/jquery-3.3.1.js"></script>
<script src="js/materialize.js"></script>
<script src="js/prototypes.js"></script>
<script src="js/front.js"></script>
<script>

    $(document).ready(function () {
        $('select').formSelect();
    });

</script>

</body>
</html>
